package org.skpublisher.Spring_Kafka_App_Two.controller;


import java.util.Random;
import java.util.concurrent.TimeUnit;

import org.skpublisher.Spring_Kafka_App_Two.model.Message;
import org.skpublisher.Spring_Kafka_App_Two.service.KafkaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/app-two/")
public class AppTwoController {
	
	@Autowired
	private KafkaService kafkaService;
		
	@RequestMapping("/test")
	private String getHi() {

		for (int i = 0; i < 50; i++)
		{
			try {
				TimeUnit.SECONDS.sleep(1);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			kafkaService.sendMessage(getSaltString());
		}
		return "Hi App 2 is working";
	}
	
	@RequestMapping(value = "/messages", method= RequestMethod.POST)
	private String addMessage(@RequestBody Message message)
	{	
		return kafkaService.sendMessage(message.msg);
	}
	
	protected String getSaltString() {
        String SALTCHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        StringBuilder salt = new StringBuilder();
        Random rnd = new Random();
        while (salt.length() < 18) { // length of the random string.
            int index = (int) (rnd.nextFloat() * SALTCHARS.length());
            salt.append(SALTCHARS.charAt(index));
        }
        String saltStr = salt.toString();
        return saltStr;

    }
}
